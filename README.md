# Awesome Gitlab

A curated list of amazingly awesome gitlab and gitlab-ci resources

## Official Documentation

- [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)
- [GitLab CI/CD Pipeline Configuration Reference](https://docs.gitlab.com/ee/ci/yaml/)
- [GitLab CI/CD environment variables](https://docs.gitlab.com/ee/ci/variables/)
- [Official Collection of .gitlab-ci.yml templates](https://gitlab.com/gitlab-org/gitlab-foss/tree/master/lib/gitlab/ci/templates)

## Gitlab API libraries
- [python-gitlab](https://github.com/python-gitlab/python-gitlab) Python package providing access to the GitLab server API and CLI tool.
- [gitlab ruby](https://github.com/NARKOZ/gitlab/) Ruby wrapper and CLI for the GitLab REST API.
- [clj-gitlab](https://gitlab.com/dzaporozhets/clj-gitlab) Clojure client for GitLab API.
- [java-gitlab-api](https://github.com/timols/java-gitlab-api) A wrapper for the Gitlab API written in Java.
- [GitLab4J API](https://github.com/gitlab4j/gitlab4j-api) GitLab4J API (gitlab4j-api) provides a full featured Java client library for working with GitLab repositories via the GitLab REST API.

## Gitlab tools

- [snippet](https://gitlab.com/zj/snippet) CLI tool to pipe to a GitLab Snippet.
- [slack-unfurl-gitlab](https://github.com/glensc/slack-unfurl-gitlab) GitLab links unfurler for slack-unfurl.
- [gitlab-unfurly](https://github.com/kiwicom/gitlab-unfurly) Serverless Slack bot for unfurling GitLab URLs.
- [gitlab-monitor](https://github.com/timoschwarzer/gitlab-monitor) Browser based pipelines monitor with failure audio indication.
- [merge-request-webhook-squasher](https://gist.github.com/AndrewFarley/9177df89666410ab45b7c248e4f24d53) [source](https://gitlab.com/gitlab-org/gitlab-foss/issues/27956#note_138567352) This simple Python code force squashes, if you call this via a webhook automatically via gitlab, then "magic" all merges will squash.

## Gitlab CI Tools

- [gitlab-ci-helpers](https://gitlab.com/morph027/gitlab-ci-helpers) Scripts to make CI usage easier: `get-last-successful-build-artifact`, `keep-tagged-artifacts` and `swarm`-related script. Also, check [wiki](https://gitlab.com/morph027/gitlab-ci-helpers/wikis/home).
- [pipeline-trigger](https://gitlab.com/finestructure/pipeline-trigger) Pipeline-trigger allows you to trigger and wait for the results of another GitLab pipeline.
- [gitlab-ci-pipeline-queue](https://gist.github.com/metanovii/afdb6a98844feff210b591a4bc249771) Trick to force waiting for previous pipeline finished.
- [gitlab-ci-monorepo](https://github.com/BastiPaeltz/gitlab-ci-monorepo) Walkthrough and examples on how GitLab 10.7+ and 11.0+ help managing CI when using monorepos.
- [gitlab-registry-cleaner](https://github.com/tnextday/gitlab-registry-cleaner) Clean up gitlab registry using the Gitlab API.
- [lab](https://github.com/zaquestion/lab) wraps Git or Hub, making it simple to clone, fork, and interact with repositories on GitLab, including seamless workflows for creating merge requests, issues and snippets.
- [gitlab-wait-for](https://gitlab.com/egeneralov/gitlab-wait-for) Tool for running gitlab pipelines, waiting for it result and playing manual jobs (and waiting for it result).

## Other Awesome Lists

Other amazingly awesome lists can be found in the [awesome-awesomeness](https://github.com/bayandin/awesome-awesomeness) list.

## Contributing

Your contributions are always welcome!
